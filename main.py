#!/usr/bin/env python

import logging
import os

from telegram import __version__ as TG_VER

from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    ConversationHandler,
    MessageHandler,
    filters,
)

group_chat = int(os.environ.get("CHAT"))
to_fix = {}

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__name__)

PROBLEMA, FOTO, POSTO, BIO = range(4)


async def init(update: Update, context):
    await update.message.reply_text(
        "Ciao! questo è il bot ufficiale della commissione bici.\n\n"
        "👉 /nuova per avviare una nuova richiesta\n\n"
        "👉 /cancella per cancellarla prima di inviarla"
    )


async def start(update: Update, context) -> int:
    """Inizia la richiesta di riparazione e chiede informazioni."""

    await update.message.reply_text(
        "Hai avviato una richiesta di riparazione,\nclicca 👉 /cancella per annullare.\n\n"
        "Invia una breve descrizione del problema che ha la tua bici."
    )

    user = update.message.from_user
    to_fix[user.first_name] = {'user': user}

    return PROBLEMA


async def problema(update: Update, context) -> int:
    """Stores the selected gender and asks for a photo."""
    user = update.message.from_user
    to_fix[user.first_name]['problema'] = update.message.text
    logger.info("Problema di %s: %s", user.first_name, update.message.text)
    await update.message.reply_text(
        "Ricevuto! Puoi mandare una foto se riteni che"
        " possa essere utile alla riparazione,\n\nclicca 👉 /salta"
        " se non serve",
        reply_markup=ReplyKeyboardRemove(),
    )

    return FOTO


async def foto(update: Update, context) -> int:
    """Stores the photo and asks for a location."""
    user = update.message.from_user
    photo_file = await update.message.photo[-1].get_file()
    await photo_file.download(user.first_name + "_photo.jpg")
    to_fix[user.first_name]['foto'] = True
    logger.info("Foto di %s: %s", user.first_name,
                user.first_name + "_photo.jpg")
    await update.message.reply_text(
        "Dove possiamo trovare la bici?\n\nConsiglio: lasciarla o nel suo"
        " posto al maschile o nelle rastrelliere e mandare una descrizione."
    )

    return POSTO


async def skip_photo(update: Update, context) -> int:
    """Salta la foto e chiede il posto"""
    user = update.message.from_user
    to_fix[user.first_name]['foto'] = False
    logger.info("%s non ha mandato una foto del problema", user.first_name)
    await update.message.reply_text(
        "Dove possiamo trovare la bici?\n\nConsiglio: lasciarla o nel suo"
        " posto al maschile o nelle rastrelliere e mandare una descrizione."
    )

    return POSTO


async def gen_and_send_report(update, context, user):
    """Genera il messaggio per la comm bici e lo invia alla chat
    indicata dalla variable d'ambente $CHAT"""

    foto_si_no = "Ha" if to_fix[user.first_name]['foto'] else "Non ha"
    username = "@{}".format(user.username) if user.username else "{} {}".format(user.first_name, user.last_name)
    link = "{}".format(user.link) if (user.link and not user.username) else ""
    problema = to_fix[user.first_name]['problema']
    posto = to_fix[user.first_name]['posto']

    msg = """{user} {link} ha effettuato una richiesta:

{problema}

Al posto: {posto}""".format(user=username,
                            link=link,
                            problema=problema,
                            posto=posto,
                            non_o_ha=foto_si_no)

    if to_fix[user.first_name]['foto']:
        photo_name = user.first_name + '_photo.jpg'
        await context.bot.send_photo(chat_id=group_chat,
                                     caption=msg,
                                     photo=open(photo_name, 'rb'))
        os.remove(photo_name)
    else:
        await context.bot.send_message(chat_id=group_chat, text=msg)

    del to_fix[user.first_name]


async def posto(update: Update, context) -> int:
    """Si segna la postazione indicata dall'utente e ringrazia per la
    segnalazione"""
    user = update.message.from_user
    user_location = update.message.text
    to_fix[user.first_name]['posto'] = user_location
    logger.info(
        "Posto della bici di %s: %s", user.first_name, user_location
    )
    await gen_and_send_report(update, context, user)
    await update.message.reply_text(
        "Grazie della segnalazione ❤️\n\nSe ci dovessero essere problemi "
        "un membro della commissione ti contatterà per ulteriori "
        "informazioni, o per avvisarti che la riparazione è avvenuta"
    )

    return ConversationHandler.END


async def cancel(update: Update, context) -> int:
    """Cancella la conversaizone e elimina i dati in memoria se
    l'utente ha iniziato richiesta, altrimenti avvisa che nessuna
    richiesta è pervenuta
    """
    user = update.message.from_user
    if user.first_name in to_fix:
        logger.info("%s ha interrotto la richiesta di riaparazione",
                    user.first_name)
        await update.message.reply_text(
            "Richiesta cancellata 💥\nSperiamo di non rivederti mai più...",
            reply_markup=ReplyKeyboardRemove()
        )
        if 'foto' in to_fix[user.first_name]:
            photo_name = user.first_name + '_photo.jpg'
            os.remove(photo_name)
        del to_fix[user.first_name]

    return ConversationHandler.END


def main() -> None:
    """Run the bot."""
    # Create the Application and pass it your bot's token.
    application = Application.builder().token(os.environ.get("TOKEN")).build()

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("nuova", start)],
        states={
            PROBLEMA: [MessageHandler(filters.TEXT & ~filters.COMMAND, problema)],
            FOTO: [MessageHandler(filters.PHOTO, foto), CommandHandler("salta", skip_photo)],
            POSTO: [
                MessageHandler(filters.TEXT & ~filters.COMMAND, posto),
            ]
            # BIO: [MessageHandler(filters.TEXT & ~filters.COMMAND, bio)],
        },
        fallbacks=[CommandHandler("cancella", cancel)],
    )

    application.add_handler(CommandHandler('start', init))
    application.add_handler(conv_handler)

    # Run the bot until the user presses Ctrl-C
    application.run_polling()


if __name__ == "__main__":
    main()
